﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Ioc;
using sugar_clicker.Services.Interface;
using sugar_clicker.ViewModels;
using static sugar_clicker.Classes.NativeMethods;

namespace sugar_clicker.Views
{
    public partial class MainWindow
    {
        private Point _start;

        private int _recentClickCount;

        private DateTime _lastClicked;

        private Timer _timer;

        private bool _forceClose;

        // 

        public MainWindow()
        {
            if (AlreadyOpen)
            {
                Refocus();
                Close();
            }

            else
                InitializeComponent();
        }

        // 

        private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            _start = e.GetPosition(null);

            if ((_lastClicked - DateTime.Now).TotalSeconds < 1.8)
            {
                _recentClickCount++;
                _timer?.Stop();
                _timer = new Timer {AutoReset = false, Interval = 1800};
                _timer.Elapsed += (o, args) => _recentClickCount = 0;
                _timer.Enabled = true;
            }
            else
                _recentClickCount = 1;

            if (_recentClickCount == 10)
            {
                Console.Beep(200, 100);
                Console.Beep(200, 100);
                Console.Beep(500, 300);
                Process.Start("https://www.youtube.com/channel/UCI0-Q4A4oolOq80zrHZeM-Q");
                _recentClickCount = 0;
            }
             
            _lastClicked = DateTime.Now;
        }

        private void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
            var position = e.GetPosition(null);
            var difference = _start - position;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(difference.X) > SystemParameters.MinimumHorizontalDragDistance ||
                 Math.Abs(difference.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                DragMove();
                _recentClickCount = 0;
                _timer?.Stop();
                _timer = null;
            }
        }

        private void OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SugarmistButton.IsChecked ^= true;
            Topmost ^= true;
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is MainWindowViewModel vm)
                vm.LoadedCommand.Execute(null);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // force method to abort - we'll force a close explicitly
            e.Cancel = true;

            if (_forceClose)
            {
                // cleanup code already ran - shut down
                e.Cancel = false;
                return;
            }

            // execute shutdown logic - Call CloseForced() to actually close window
            Dispatcher.InvokeAsync(ShutdownApplication, DispatcherPriority.Normal);
        }

        public void CloseForced()
        {
            _forceClose = true;
            Close();
        }

        private async Task ShutdownApplication()
        {
            try
            {
                if (DataContext is MainWindowViewModel vm && vm.RankingEverEnabled)
                    await SimpleIoc.Default.GetInstance<IRankingService>().Withdraw().ConfigureAwait(true);
                CloseForced();
            }
            catch (Exception)
            {
                CloseForced();
            }
        }
    }
}

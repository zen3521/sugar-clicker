﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using sugar_clicker.Services;
using sugar_clicker.Services.Interface;
using sugar_clicker.ViewModels;

namespace sugar_clicker
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IRankingService, RankingService>();
            SimpleIoc.Default.Register<MainWindowViewModel>();
        }

        public MainWindowViewModel Main => ServiceLocator.Current.GetInstance<MainWindowViewModel>();
    }
}
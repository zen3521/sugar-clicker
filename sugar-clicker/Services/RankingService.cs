﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using sugar_clicker.Services.Interface;

namespace sugar_clicker.Services
{
    public class RankingService : IRankingService
    {
        private readonly HttpClient _client = new HttpClient(new HttpClientHandler
        {
            AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
        });

        private const string Url = "https://sugarmist.herokuapp.com";

        private string Id { get; } = Guid.NewGuid().ToString();

        private DateTime Time { get; set; } = DateTime.Now;
        
        // 

        public void Reset() => Time = DateTime.Now;

        public async Task<(string, string)> Rank()
        {
            try
            {
                var pairs = new Dictionary<string, string>
                {
                    {"id", Id},
                    {"seconds", SecondsAlive().ToString()}
                };
                var payload = new FormUrlEncodedContent(pairs);
                var request = await _client.PostAsync(Url + "/rank", payload);
                var response = JObject.Parse(await request.Content.ReadAsStringAsync());
                return (response["position"].Value<string>(), response["count"].Value<string>());
            }

            catch (Exception)
            {
                // pass
            }

            return ("", "");
        }

        public async Task Withdraw()
        {
            try
            {
                var pairs = new Dictionary<string, string>
                {
                    {"id", Id},
                };
                await _client.PostAsync(Url + "/withdraw", new FormUrlEncodedContent(pairs));
            }

            catch (Exception)
            {
                // pass
            }
        }

        // 

        private int SecondsAlive() => (int) (DateTime.Now - Time).TotalSeconds;
    }
}
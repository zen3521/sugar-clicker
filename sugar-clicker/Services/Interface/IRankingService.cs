﻿using System.Threading.Tasks;

namespace sugar_clicker.Services.Interface
{
    public interface IRankingService
    {
        Task<(string, string)> Rank();
        Task Withdraw();
        void Reset();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using sugar_clicker.Services.Interface;

namespace sugar_clicker.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IRankingService _ranking;

        private readonly Dictionary<string, DateTime> _timers 
            = new Dictionary<string, DateTime> { { "ranking", DateTime.Now } };

        // 

        public MainWindowViewModel(IRankingService ranking) => _ranking = ranking;

        // 

        public bool RankingEverEnabled { get; private set; }

        public string Rank { get; set; } = "";

        public string Count { get; set; } = "";

        public bool RankEnabled { get; set; }

        public RelayCommand ExitCommand => new RelayCommand(() => Application.Current.MainWindow?.Close());

        public RelayCommand LoadedCommand => new RelayCommand(Loaded);

        public RelayCommand ToggleRankCommand => new RelayCommand(ToggleRank);

        // 

        private void ToggleRank()
        {
            // If going from disabled->enabled, reset current ranking
            if (!RankEnabled)
                _ranking.Reset();

            RankEnabled ^= true;
            Rank = RankEnabled ? "..." : "";
            Count = "";
            _timers["ranking"] = DateTime.MinValue;
        }

        private async void Loaded()
        {
            while (true)
            {
                if (RankEnabled && DateTime.Now > _timers["ranking"])
                {
                    try
                    {
                        (Rank, Count) = await _ranking.Rank();
                        RankingEverEnabled = true;
                        _timers["ranking"] = DateTime.Now.AddMinutes(5);
                    }

                    // Retry in one minute on failure
                    catch (Exception)
                    {
                        _timers["ranking"] = DateTime.Now.AddMinutes(1);
                    }
                }
                
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }
    }
}